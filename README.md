# KISSBAC Design Notes

KISSBAC, the (hopefully) stupidly simple Building Automation Controller.



## Hardware Overview

Base board supports 8 channels of I/O. Channels front-ends are modular, and are installed on separate daughter boards. The daugher boards are provided with +5v and +3.3v, and I2C signalling to the CPU. Each daughter card has a dedicated I2C address space, provided by an I2C multiplexer installed on the main board. The boards contain I2C eeprom to provide enumeration information to the CPU. Base boards can be chained together, omitting the CPU module and ethernet/USB connectors on the second board for low-cost I/O expansion via I2C.

## Software Overview

At the basic level, node.js wrappers will be provided to allow for development of building automation logic using Javascript, as well as a RESTful API that will provide access to capabilites of the board as well. Add to this an event driven automation engine so that a single KISSBAC controller can run without any outside support. Plans are to develop a web-based configuration system (ember? angular? backbone?) that will wrap the API in a system that allows for almost-drag-n-drop ease of configuring automation actions.

Reporting! Data feeds: Elastic Search, statsd. Distributed event processing spread across multiple controllers, cloud integration to facilitate campus-wide or organization wide integration of automation and data collection. The possibilities are endless.

## Hardware Ruminations

Back down to earth, the nuts and bolts of it all...

#### CPU Module
Linkit Smart 7688
#### I2C Multiplexer
PCA9548A
#### RTC
Maxim DS3231
